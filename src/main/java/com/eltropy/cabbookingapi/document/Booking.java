package com.eltropy.cabbookingapi.document;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;

@Document
@Data
@AllArgsConstructor
public class Booking {

    @Id
    private long id;
    private String name;
    private double cost;
    private LocalDateTime start;
    
    

  
}
