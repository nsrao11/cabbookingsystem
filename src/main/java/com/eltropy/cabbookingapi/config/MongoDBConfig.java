package com.eltropy.cabbookingapi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.eltropy.cabbookingapi.repository.BookingRepository;

@EnableMongoRepositories(basePackageClasses = BookingRepository.class)
@Configuration
public class MongoDBConfig {


	/*
	 * @Bean CommandLineRunner commandLineRunner(BookingRepository
	 * bookingRepository) { return strings -> { bookingRepository.save(new
	 * Booking(1, "Peter", "Development", 3000L)); bookingRepository.save(new
	 * Booking(2, "Sam", "Operations", 2000L)); }; }
	 */

}
