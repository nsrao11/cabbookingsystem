package com.eltropy.cabbookingapi.repository.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.eltropy.cabbookingapi.document.Booking;
import com.eltropy.cabbookingapi.model.BookingDto;
import com.eltropy.cabbookingapi.repository.BookingRepository;


@Component
public class BookingServiceImpl implements BookingService {

	@Autowired
	private BookingRepository bookingRepository;


	public BookingServiceImpl() {

	}


	/**
	 * Book a taxi.
	 *
	 * @param bookingDto booking data transfer object. Requires: - username username
	 *                   of passenger. - numberPassengers number of passengers. -
	 *                   pickup location - destination location
	 * @throws AccountAuthenticationFailed       if account authentication fails.
	 * @throws RouteNotFoundException            route not found.
	 * @throws InvalidGoogleApiResponseException invalid response from Google
	 *                                           (Invalid JSON as there API has
	 *                                           changed).
	 * @throws InvalidBookingException           invalid booking e.g. user has
	 *                                           active bookings.
	 */

	@Override
	public Booking makeBooking(BookingDto bookingDto) {

		Booking booking = null;
    try {
		
    	this.bookingRepository.save(bookingDto);

		return booking;
		
	    }catch(Exception e) {
			throw new RuntimeException("Booking Failed!");
		}

      }


  }
