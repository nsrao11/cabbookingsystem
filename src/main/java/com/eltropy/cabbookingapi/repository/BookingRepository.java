package com.eltropy.cabbookingapi.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.eltropy.cabbookingapi.model.BookingDto;

public interface BookingRepository extends MongoRepository<BookingDto, Integer> {
}
