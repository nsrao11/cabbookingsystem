package com.eltropy.cabbookingapi.rest;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eltropy.cabbookingapi.document.Booking;
import com.eltropy.cabbookingapi.model.BookingDto;
import com.eltropy.cabbookingapi.repository.service.BookingServiceImpl;
import com.eltropy.cabbookingapi.util.DataMapper;


@RestController
@RequestMapping("/bookings")
public class BookingController {
	
	private static final Logger LOGGER = Logger.getLogger(BookingController.class.getName());

	private  DataMapper mapper;
	
	@Autowired
	private BookingServiceImpl bookingServiceImpl;

	public BookingController() {
		this.mapper = DataMapper.getInstance();
	}
	
	
    @PostMapping(value="/makeBooking")
    public Booking makeBooking(@RequestBody BookingDto bookingDto)  throws Exception{
    	
		Booking booking=bookingServiceImpl.makeBooking(bookingDto);
    	return booking;
    	
    }
}
